#!/bin/sh
dir="${1}"
if [ -z "${dir}" ] || [ ! -d "${dir}" ]; then
    echo must supply valid directory name
    exit 1
fi
cd "${dir}" || exit

function getnames () {
    for prefix in "Fuel Storage" "Compressed Fuel Storage System" \
                  "Engineering Spaces" "Crew Quarters" "Cryogenic Transport" \
                  "Cargo Hold" "Troop Transport Bay" \
                  "Troop Transport Boarding Bay" "Troop Transport Drop Bay"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${prefix}'
       || (SELECT char
           FROM   (SELECT Size AS value,
                          char(8320 + row_number() OVER (ORDER BY Size)) AS char
                   FROM   FCT_ShipDesignComponents
                   WHERE  Name LIKE '${prefix}%')
           WHERE  value = Size)
       || ' ('
       || replace(Size * 50, '.0', '')
       || 't)' AS NewName
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '${prefix}%';
EOF
    done

    for suffix in "Maintenance Storage Bay"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${suffix}'
       || (SELECT char
           FROM   (SELECT Size AS value,
                          char(8320 + row_number() OVER (ORDER BY Size)) AS char
                   FROM   FCT_ShipDesignComponents
                   WHERE  Name LIKE '%${suffix}')
           WHERE  value = Size)
       || ' ('
       || replace(Size * 50, '.0', '')
       || 't)' AS NewName
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${suffix}';
EOF
    done

    for suffix in $(seq --format=ECM-%.0f 1 10) $(seq --format=ECCM-%.0f 1 10); do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${suffix}'
       || (SELECT char
           FROM   (SELECT Size AS value,
                          char(8320 + row_number() OVER (ORDER BY Size DESC)) AS char
                   FROM   FCT_ShipDesignComponents
                   WHERE  Name LIKE '%${suffix}')
           WHERE  value = Size)
       || ' ('
       || replace(Size * 50, '.0', '')
       || 't)' AS NewName
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${suffix}';
EOF
    done

    for infix in "Jump Point Stabilisation Module"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${infix}'
       || (SELECT char
           FROM   (SELECT ComponentValue AS value,
                          char(8320 + row_number() OVER (ORDER BY ComponentValue DESC)) AS char
                   FROM   FCT_ShipDesignComponents
                   WHERE  Name LIKE '%${infix}%')
           WHERE  value = ComponentValue)
       || ' ('
       || cast(ComponentValue AS INTEGER)
       || 'days, '
       || replace(Size * 50, '.0', '')
       || 't)' AS NewName
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${infix}%';
EOF
    done
}

rm -f -- *.sql.tmp uninstall.sql rename.sql 2>&-
getnames | sort -t $'\t' -k1n | while IFS=$'\t' read -r id old new; do
    echo "id='${id}', old='${old}', new=${new}"
    echo "UPDATE FCT_ShipDesignComponents SET Name = '${old}' WHERE SDComponentID = ${id};" >> uninstall.sql
    echo "UPDATE FCT_ShipDesignComponents SET Name = '${new}' WHERE SDComponentID = ${id};" >> rename.sql
done
